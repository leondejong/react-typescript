import styled, { StyleSheetManager } from "styled-components";
import { BrowserRouter } from "react-router-dom";

import {
  StateProvider,
  persistedState,
  persistedReducer,
  state as defaultState,
} from "./state";
import { Layout } from "./components";

// Check if Object, excludes: Array, Date, Null, Set, String, Undefined
function isObject(v: unknown): boolean {
  return Object.prototype.toString.call(v) === "[object Object]";
}

const shouldForwardProp = (name: string, target: unknown) => {
  return typeof target === "string"
    ? !["color", "background", "border", "label", "colorActive"].includes(name)
    : true;
};

const Application = styled.div`
  display: flex;
  justify-content: center;
  min-height: 100vh;
`;

const App = () => (
  <StateProvider
    state={isObject(persistedState) ? persistedState : defaultState}
    reducer={persistedReducer}
  >
    <BrowserRouter>
      <StyleSheetManager shouldForwardProp={shouldForwardProp}>
        <Application>
          <Layout />
        </Application>
      </StyleSheetManager>
    </BrowserRouter>
  </StateProvider>
);

export default App;
